FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} paiement-ms.jar
ENTRYPOINT ["java","-jar","/paiement-ms.jar"]
